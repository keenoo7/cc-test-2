

# Complete Country Site


## Install Instructions


- open terminal, check if node is installed by typing: node -v
- if it is not, go here: https://nodejs.org/
- install node.js
- open terminal, type : node -v 
- a version should be visible
- in terminal, install gulp globally(for first time of gulp on computer only) type: sudo npm install gulp -g
- in terminal, cd into project, type: npm init
- install gulp plugins below one at a time: 

- npm install gulp --save-dev
- npm install gulp-sass gulp-concat --save-dev
- npm install gulp-autoprefixer --save-dev
- npm install gulp-babel babel-preset-env babel-core --save-dev


### run gulp
### preview in browser